<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wea', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dataid',13)->unique();
            $table->string('akz');
            $table->string('serialnr');
            $table->tinyInteger('weanr');
            $table->string('gkRight')->nullable();
            $table->string('gkHeight')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->tinyInteger('mahd')->default(0);
            $table->tinyInteger('mahd_days')->nullable();
            $table->integer('mahd_offset_sunrise')->default(0);
            $table->integer('mahd_offset_sunset')->default(0);
            $table->tinyInteger('kranich')->default(0);
            $table->string('kranich_start');
            $table->string('kranich_stop');
            $table->tinyInteger('military')->default(0);
            $table->tinyInteger('bird')->default(0);
            $table->tinyInteger('update')->default(0)->unsigned();
            $table->tinyInteger('parsed')->default(0)->unsigned();
            $table->tinyInteger('deleted')->default(0)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wea');
    }
}
