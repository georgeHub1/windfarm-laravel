<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkzTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akz', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dataid',13)->unique();
            $table->string('akz')->unique();
            $table->string('name')->nullable();
            $table->string('location')->nullable();
            $table->string('connector')->nullable();
            $table->tinyInteger('update')->default(0)->unsigned();
            $table->tinyInteger('parsed')->default(0)->unsigned();
            $table->tinyInteger('deleted')->default(0)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akz');
    }
}
