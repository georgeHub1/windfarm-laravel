<?php
return [
    [
        'site'  => '/',
        'title' => 'Home',
        'url'   => '/',
        'icon'  => 'fas fa-home'
    ],
    [
        'site'  => 'manageAkz',
        'title' => 'AKZ Manager',
        'url'   => '/manageAkz',
        'icon'  => 'fas fa-map-marked-alt'
    ],
    [
        'site'  => 'manageWea',
        'title' => 'WEA Manager',
        'url'   => '/manageWea',
        'icon'  => 'fas fa-broadcast-tower'
    ],
    [
        'site'  => 'register',
        'title' => 'Admin hinzufügen',
        'url'   => '/register',
        'icon'  => 'fas fa-user-shield'
    ],
    [
        'site'  => 'adminList',
        'title' => 'Administratoren',
        'url'   => '/adminList',
        'icon'  => 'fas fa-user-cog'
    ],
];