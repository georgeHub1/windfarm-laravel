@extends('fleximaus.html')
@section('title','WEA Manager')
@section('headJS')
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDu3bP5y6GErqLNItyB7UBsVeEGmTKgsn0"></script>
@endsection

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">WEA</h4>
                    <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                        <i class="fa fa-plus"></i>
                        WEA hinzufügen
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- Modal -->
                <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h5 class="modal-title">
                                    <span class="fw-mediumbold">WEA</span>
                                    <span class="fw-light">hinzufügen</span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="post" action="/addWea">
                                @csrf
                                <div class="modal-body">
                                    <p class="small"></p>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label for="selectAkz">AKZ</label>
                                                <select class="form-control" id="selectAkz" name="akz" required>
                                                    <option value="">Bitte wählen</option>
                                                    @foreach($arrAkz as $akz)
                                                        <option value="{{$akz->akz}}">{{$akz->akz}}: {{$akz->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>WEA Nummer</label>
                                                <input name="weanr" type="number" class="form-control"
                                                       placeholder="WEA Nummer" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Seriennummer</label>
                                                <input name="serialnr" type="text" class="form-control"
                                                       placeholder="Seriennummer" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-group-default">
                                                <label>GaussKrüger Rechtswert</label>
                                                <input name="gkRight" type="text" class="form-control"
                                                       placeholder="Rechtswert">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-group-default">
                                                <label>GaussKrüger Hochwwert</label>
                                                <input name="gkHeight" type="text" class="form-control"
                                                       placeholder="Hochwert">
                                            </div>
                                        </div>
                                        <h4 class="col-sm-12">Mahd Abschaltung</h4>
                                        <div class="col-sm-2">
                                            <div class="form-group form-group-default">
                                                <label>Abschaltung</label>
                                                <input type="checkbox" name="mowing" value="1" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group form-group-default">
                                                <label>Tage</label>
                                                <input name="mahd_days" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Offset Sonnenaufgang</label>
                                                <input name="mahd_offset_sunrise" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Offset Sonnenuntergang</label>
                                                <input name="mahd_offset_sunset" type="number" class="form-control">
                                            </div>
                                        </div>
                                        <h4 class="col-sm-12">Sonstige Abschaltungen</h4>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Abschaltung Kranich</label>
                                                <input type="checkbox" name="crane" value="1" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Abschaltung Militär</label>
                                                <input type="checkbox" name="military" value="1" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Abschaltung Vogel</label>
                                                <input type="checkbox" name="bird" value="1" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-group-default">
                                                <label>Abschaltung Milan</label>
                                                <input type="checkbox" name="milan" value="1" data-toggle="toggle" data-onstyle="success">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="submit" class="btn btn-primary">Speichern</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Schließen</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->
                <div class="table-responsive">
                    <table id="multi-filter-select" class="display table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>AKZ</th>
                            <th>WEA Nummer</th>
                            <th>Seriennummer</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>AKZ</th>
                            <th>WEA Numme</th>
                            <th>Seriennummer</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($arrData as $wea)
                            <tr>
                                <td>{{$wea->akz}}:{{@$arrAkz[$wea->akz]['name']}}</td>
                                <td>{{$wea->weanr}}</td>
                                <td>{{$wea->serialnr}}</td>
                                <td class="d-flex justify-content-around">
                                    <a href="" data-toggle="modal" data-target="#Modal{{$wea->id}}"><i class="far fa-edit"></i></a>
                                    <div class="modal fade" id="Modal{{$wea->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-bd">
                                                    <h5 class="modal-title">
                                                        <span class="fw-mediumbold">WEA</span>
                                                        <span class="fw-light">ändern</span>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post" action="/editWea">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$wea->id}}">
                                                    <input type="hidden" name="update" value="1">
                                                    <div class="modal-body">
                                                        <p class="small"></p>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group form-group-default">
                                                                    <label for="selectAkz">AKZ</label>
                                                                    <select class="form-control" id="selectAkz" name="akz" required>
                                                                        <option value="">Bitte wählen</option>
                                                                        @foreach($arrAkz as $akz)
                                                                            <option value="{{$akz->akz}}" @if($wea->akz == $akz->akz)selected @endif >{{$akz->akz}}: {{$akz->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>WEA Nummer</label>
                                                                    <input name="weanr" type="number" class="form-control"
                                                                           value="{{$wea->weanr}}" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>Seriennummer</label>
                                                                    <input name="serialnr" type="text" class="form-control"
                                                                           value="{{$wea->serialnr}}" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>Gauss-Krüger Rechtswert</label>
                                                                    <input name="gkRight" type="text" class="form-control"
                                                                           value="{{$wea->gkRight}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>Gauss-Krüger Hochwert</label>
                                                                    <input name="gkHeight" type="text" class="form-control"
                                                                           value="{{$wea->gkHeight}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>WGS84 Latitude</label>
                                                                    <input name="gkHeight" type="text" class="form-control px-2"
                                                                           value="{{$wea->latitude}}" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group form-group-default">
                                                                    <label>WGS84 Longitude</label>
                                                                    <input name="gkHeight" type="text" class="form-control px-2"
                                                                           value="{{$wea->longitude}}" disabled>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-sm-12">Mahd Abschaltung</h4>
                                                            <div class="col-sm-2">
                                                                <div class="form-group form-group-default">
                                                                    <label>Abschaltung</label>
                                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="success" name="mowing" value="1" {{$wea->mowing == 0 ? '' : 'checked'}}>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="form-group form-group-default">
                                                                    <label>Tage</label>
                                                                    <input name="mahd_days" type="number" value="{{$wea->mahd_days}}" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>Offset Sonnenaufgang</label>
                                                                    <input name="mahd_offset_sunrise" value="{{$wea->mahd_offset_sunrise}}" type="number" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group form-group-default">
                                                                    <label>Offset Sonnenuntergang</label>
                                                                    <input name="mahd_offset_sunset" value="{{$wea->mahd_offset_sunset}}" type="number" class="form-control">
                                                                </div>
                                                            </div>
                                                            <h4 class="col-sm-12">Sonstige Abschaltungen</h4>
                                                            <div class="col-sm-3">
                                                                <div class="form-group form-group-default">
                                                                    <label>Abschaltung Kranich</label>
                                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="success"  name="crane" value="1" {{$wea->crane == 0 ? '' : 'checked'}}>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group form-group-default">
                                                                    <label>Abschaltung Militär</label>
                                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="success"  name="military" value="1" {{$wea->military == 0 ? '' : 'checked'}}>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group form-group-default">
                                                                    <label>Abschaltung Vogel</label>
                                                                    <input type="checkbox" data-toggle="toggle" data-onstyle="success" name="bird" value="1" {{$wea->bird == 0 ? '' : 'checked'}}>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group form-group-default">
                                                                    <label>Abschaltung Milan</label>
                                                                    <input type="checkbox" name="milan" data-toggle="toggle" data-onstyle="success" value="1" {{$wea->milan == 0 ? '' : 'checked'}}>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if(!empty($wea->latitude) && !empty($wea->longitude))
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-12" id="{{$wea->id}}gmap" style="height: 400px !important;"></div>
                                                                </div>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function() {
                                                                    var map = new GMaps({
                                                                        div: '#{{$wea->id}}gmap',
                                                                        lat: '{{$wea->latitude}}',
                                                                        lng: '{{$wea->longitude}}',
                                                                        mapType: 'satellite',
                                                                        zoom: 18,
                                                                    });

                                                                    map.addMarker({
                                                                        lat: '{{$wea->latitude}}',
                                                                        lng: '{{$wea->longitude}}',
                                                                        title: '{{$wea->serialnr}}',
                                                                        infoWindow: {
                                                                            content: '<p>WEA {{$wea->weanr}}<br>AKZ {{$arrAkz[$wea->akz]['name']}}</p>'
                                                                        }
                                                                    });
                                                                    $('#{{$wea->id}}gmap').height(250);
                                                                    $('#{{$wea->id}}gmap').css('width','');
                                                                });
                                                            </script>
                                                        @endif
                                                    </div>
                                                    <div class="modal-footer no-bd">
                                                        <button type="submit" class="btn btn-primary">Speichern</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Schließen</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="/delWea?id={{$wea->id}}"><i style="color: #FF0000;" class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('bodyJS')
<script >
    $(document).ready(function() {
        $('#multi-filter-select').DataTable( {
            "pageLength": 25,
            "language": {
                "url": "{{asset('js/plugin/datatables/german.json')}}"
            },
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });
    });
</script>
@endsection
