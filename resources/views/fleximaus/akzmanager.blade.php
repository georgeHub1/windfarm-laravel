@extends('fleximaus.html')
@section('title','AKZ Manager')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">AKZ</h4>
                    <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal">
                        <i class="fa fa-plus"></i>
                        AKZ hinzufügen
                    </button>
                </div>
            </div>
            <div class="card-body">
                <!-- Modal -->
                <div class="modal fade" id="addRowModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h5 class="modal-title">
                                    <span class="fw-mediumbold">AKZ</span>
                                    <span class="fw-light">hinzufügen</span>
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="post" action="/addAkz">
                                @csrf
                                <div class="modal-body">
                                    <p class="small"></p>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>AKZ</label>
                                                <input name="akz" type="number" class="form-control"
                                                       placeholder="AKZ">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Windparkname</label>
                                                <input name="name" type="text" class="form-control"
                                                       placeholder="Windparkname">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Standort</label>
                                                <input name="location" type="text" class="form-control"
                                                       placeholder="Standort">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-group-default">
                                                <label>Connector</label>
                                                <input name="connector" type="text" class="form-control"
                                                       placeholder="Connector">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-bd">
                                    <button type="submit" id="addRowButton" class="btn btn-primary">Speichern</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Schließen</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->
                <div class="table-responsive">
                    <table id="multi-filter-select" class="display table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>AKZ</th>
                            <th>Name</th>
                            <th>WEA</th>
                            <th>Connector</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>AKZ</th>
                            <th>Name</th>
                            <th>WEA</th>
                            <th>Connector</th>
                            <th>&nbsp;</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($arrData as $akz)
                            <tr>
                                <td>{{$akz->akz}}</td>
                                <td>{{$akz->name}}</td>
                                <td>&nbsp;</td>
                                <td>{{$akz->connector}}</td>
                                <td class="d-flex justify-content-around">
                                    <a href="" data-toggle="modal" data-target="#Modal{{$akz->id}}"><i class="far fa-edit"></i></a>
                                    <div class="modal fade" id="Modal{{$akz->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header no-bd">
                                                    <h5 class="modal-title">
                                                        <span class="fw-mediumbold">AKZ</span>
                                                        <span class="fw-light">ändern</span>
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post" action="/editAkz">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$akz->id}}">
                                                    <input type="hidden" name="update" value="1">
                                                    <div class="modal-body">
                                                        <p class="small"></p>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default">
                                                                    <label>AKZ</label>
                                                                    <input name="akz" type="number" class="form-control"
                                                                           value="{{$akz->akz}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default">
                                                                    <label>Windparkname</label>
                                                                    <input name="name" type="text" class="form-control"
                                                                           value="{{$akz->name}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default">
                                                                    <label>Standort</label>
                                                                    <input name="location" type="text" class="form-control"
                                                                           value="{{$akz->location}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-group form-group-default">
                                                                    <label>Connector</label>
                                                                    <input name="connector" type="text" class="form-control"
                                                                           value="{{$akz->connector}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer no-bd">
                                                        <button type="submit" id="addRowButton" class="btn btn-primary">Speichern</button>
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Schließen</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="/delAkz?id={{$akz->id}}"><i style="color: #FF0000;" class="fas fa-trash-alt"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('bodyJS')
<script >
    $(document).ready(function() {
        $('#multi-filter-select').DataTable( {
            "pageLength": 25,
            "language": {
                "url": "{{asset('js/plugin/datatables/german.json')}}"
            },
            initComplete: function () {
                this.api().columns().every( function () {
                    var column = this;
                    var select = $('<select class="form-control"><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        });
    });
</script>
@endsection