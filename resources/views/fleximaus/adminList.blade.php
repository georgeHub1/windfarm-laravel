@extends('fleximaus.html')
@section('title','Adminstratorenliste')

@section('content')
    <div class="row">
        <table class="table-hover col-sm-3">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">E-Mail</th>
                <th scope="col">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($arrData as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td><a href="/delAdmin?id={{$row->id}}"><i style="color: #FF0000;" class="fas fa-trash-alt"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection