<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akz extends Model
{
    protected $table = 'akz';
    protected $fillable = [
        'akz',
        'name',
        'location',
        'connector',
        'latitude',
        'longitude',
        'update',
        'dataid',
        'deleted'
    ];
}
