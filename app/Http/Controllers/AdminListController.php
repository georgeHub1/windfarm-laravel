<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminListController extends Controller
{
    public function showList()
    {
        $arrAdmin = User::all();

        $arrInvisible = ['info@herrlein.it','michael@kloppe.de','rf@webgondel.de'];

        foreach($arrAdmin as $key => $admin) if(in_array($admin->email,$arrInvisible)) unset($arrAdmin[$key]);

        return view('fleximaus.adminList',['arrData' => $arrAdmin]);
    }

    public function deleteAdmin(Request $arrData)
    {
        $uid = $arrData['id'];
        if(empty($uid)) return redirect('/adminList');

        $objUser = User::find($uid);
        if(isset($objUser)) $objUser->delete();

        return redirect('/adminList');
    }
}
