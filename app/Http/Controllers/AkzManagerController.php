<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Akz;
use App\Wea;

class AkzManagerController extends Controller
{
    public function manage()
    {
        $arrAkz = Akz::where('deleted',0)->get();
        return view('fleximaus.akzmanager',['arrData' => $arrAkz]);
    }

    public function addAkz(Request $arrData)
    {
        $arrData['dataid'] = uniqid();
        Akz::create($arrData->all());

        return redirect()->route('akzManager');
    }

    public function delAkz(Request $arrData)
    {
        $akzId = $arrData['id'];
        if(empty($akzId)) return redirect()->route('akzManager');

        $objAkz = Akz::find($akzId);
        if(isset($objAkz))
        {
            $objAkz->deleted = 1;
            $objAkz->save();
        }

        return redirect()->route('akzManager');
    }

    public function editAkz(Request $arrData)
    {
        $objData = Akz::find($arrData['id']);
        if(!$objData) return redirect()->route('akzManager');
        $arrInput = $arrData->all();
        $arrInput['update'] = 1;
        $objData->fill($arrInput)->save();
        return redirect()->route('akzManager');
    }

    public function addWea()
    {

    }

    public function createWea(Request $arrData)
    {

    }
}
