<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Akz;
use App\Wea;

class WeaManagerController extends Controller
{
    public function manage()
    {
        $arrWea = Wea::where('deleted',0)->get();
        $arrAkz = Akz::select('id','akz','name')->where('deleted',0)->get();
        foreach($arrAkz as $key => $row)
        {
            $arrAkz[$row->akz] = $row;
            unset($arrAkz[$key]);
        }
        return view('fleximaus.weamanager',['arrData' => $arrWea, 'arrAkz' => $arrAkz]);
    }

    public function addWea(Request $arrData)
    {
        $arrData = $arrData->all();
        $arrData['dataid'] = uniqid();
        // Set latitude and longitude
        if(!empty($arrData['gkRight']) && !empty($arrData['gkHeight']))
        {
            $arrWGS = $this->gk2wgs($arrData['gkRight'],$arrData['gkHeight']);
            if($arrWGS) $arrData = array_merge($arrData,$arrWGS);
        }
        // sanitize offsets
        if(!is_int($arrData['mahd_offset_sunrise'])) $arrData['mahd_offset_sunrise'] = 0;
        if(!is_int($arrData['mahd_offset_sunset'])) $arrData['mahd_offset_sunset'] = 0;

        Wea::create($arrData);
        return redirect()->route('weaManager');
    }

    public function editWea(Request $arrData)
    {
        $objData = Wea::find($arrData['id']);
        if(!$objData) return redirect()->route('weaManager');
        $arrInput = $arrData->all();
        $arrInput['update'] = 1;

        if(!isset($arrInput['mowing'])) $arrInput['mowing'] = 0;
        if(!isset($arrInput['crane'])) $arrInput['crane'] = 0;
        if(!isset($arrInput['military'])) $arrInput['military'] = 0;
        if(!isset($arrInput['bird'])) $arrInput['bird'] = 0;
        if(!isset($arrInput['milan'])) $arrInput['milan'] = 0;

        if(!empty($arrInput['gkRight']) && !empty($arrInput['gkHeight']))
        {
            $arrWGS = $this->gk2wgs($arrInput['gkRight'],$arrInput['gkHeight']);
            if($arrWGS) $arrInput = array_merge($arrInput,$arrWGS);
        }
        $objData->fill($arrInput)->save();
        return redirect()->route('weaManager');
    }

    public function delWea(Request $arrData)
    {
        $weaId = $arrData['id'];

        if(empty($weaId)) return redirect()->route('weaManager');

        $objWea = Wea::find($weaId);
        if(isset($objWea))
        {
            $objWea->deleted = 1;
            $objWea->save();
        }

        return redirect()->route('weaManager');
    }

    private function gk2wgs($gkRight, $gkHeight)
    {
        $arrPot = $this->gk2geo($gkRight,$gkHeight);
        if(!$arrPot) return false;
        $arrWGS = $this->pot2wgs($arrPot['latitude'], $arrPot['longitude']);
        return $arrWGS;
    }

    private function gk2geo($gkRight, $gkHeight)
    {
        /* Copyright (c) 2007, HELMUT H. HEIMEIER
           Permission is hereby granted, free of charge, to any person obtaining a
           copy of this software and associated documentation files (the "Software"),
           to deal in the Software without restriction, including without limitation
           the rights to use, copy, modify, merge, publish, distribute, sublicense,
           and/or sell copies of the Software, and to permit persons to whom the
           Software is furnished to do so, subject to the following conditions:
           The above copyright notice and this permission notice shall be included
           in all copies or substantial portions of the Software.

           Changes by Martin Herrlein: Input and return values. Removing globals.
           Returns array
        */

        /* Die Funktion wandelt GK Koordinaten in geographische Koordinaten
           um. Rechtswert rw und Hochwert hw müssen gegeben sein.
           Berechnet werden geographische Länge lp und Breite bp
           im Potsdam Datum.*/

        $rw = $gkRight;
        $hw = $gkHeight;

        // Rechtswert rw und Hochwert hw im Potsdam Datum
        if ($rw == "" || $hw == "") return false;

        $rw = intval($rw);
        $hw = intval($hw);

        // Potsdam Datum
        // Große Halbachse a und Abplattung f
        $a = 6377397.155;
        $f = 3.34277321e-3;
        // Polkrümmungshalbmesser c
        $c = $a / (1 - $f);

        // Quadrat der zweiten numerischen Exzentrizität
        $ex2 = (2 * $f - $f * $f) / ((1 - $f) * (1 - $f));
        $ex4 = $ex2 * $ex2;
        $ex6 = $ex4 * $ex2;
        $ex8 = $ex4 * $ex4;

        // Koeffizienten zur Berechnung der geographischen Breite aus gegebener
        // Meridianbogenlänge
        $e0 = $c * (pi() / 180) * (1 - 3 * $ex2 / 4 + 45 * $ex4 / 64 - 175 * $ex6 / 256 + 11025 * $ex8 / 16384);
        $f2 = (180 / pi()) * (3 * $ex2 / 8 - 3 * $ex4 / 16 + 213 * $ex6 / 2048 - 255 * $ex8 / 4096);
        $f4 = (180 / pi()) * (21 * $ex4 / 256 - 21 * $ex6 / 256 + 533 * $ex8 / 8192);
        $f6 = (180 / pi()) * (151 * $ex6 / 6144 - 453 * $ex8 / 12288);

        // Geographische Breite bf zur Meridianbogenlänge gf = hw
        $sigma = $hw / $e0;
        $sigmr = $sigma * pi() / 180;
        $bf = $sigma + $f2 * sin(2 * $sigmr) + $f4 * sin(4 * $sigmr) + $f6 * sin(6 * $sigmr);

        // Breite bf in Radianten
        $br = $bf * pi() / 180;
        $tan1 = tan($br);
        $tan2 = $tan1 * $tan1;
        $tan4 = $tan2 * $tan2;

        $cos1 = cos($br);
        $cos2 = $cos1 * $cos1;

        $etasq = $ex2 * $cos2;

        // Querkrümmungshalbmesser nd
        $nd = $c / sqrt(1 + $etasq);
        $nd2 = $nd * $nd;
        $nd4 = $nd2 * $nd2;
        $nd6 = $nd4 * $nd2;
        $nd3 = $nd2 * $nd;
        $nd5 = $nd4 * $nd;

        //  Längendifferenz dl zum Bezugsmeridian lh
        $kz = intval($rw / 1e6);
        $lh = $kz * 3;
        $dy = $rw - ($kz * 1e6 + 500000);
        $dy2 = $dy * $dy;
        $dy4 = $dy2 * $dy2;
        $dy3 = $dy2 * $dy;
        $dy5 = $dy4 * $dy;
        $dy6 = $dy3 * $dy3;

        $b2 = -$tan1 * (1 + $etasq) / (2 * $nd2);
        $b4 = $tan1 * (5 + 3 * $tan2 + 6 * $etasq * (1 - $tan2)) / (24 * $nd4);
        $b6 = -$tan1 * (61 + 90 * $tan2 + 45 * $tan4) / (720 * $nd6);

        $l1 = 1 / ($nd * $cos1);
        $l3 = -(1 + 2 * $tan2 + $etasq) / (6 * $nd3 * $cos1);
        $l5 = (5 + 28 * $tan2 + 24 * $tan4) / (120 * $nd5 * $cos1);

        // Geographischer Breite bp und Länge lp als Funktion von Rechts- und Hochwert
        $bp = $bf + (180 / pi()) * ($b2 * $dy2 + $b4 * $dy4 + $b6 * $dy6);
        $lp = $lh + (180 / pi()) * ($l1 * $dy + $l3 * $dy3 + $l5 * $dy5);

        if ($lp < 5 || $lp > 16 || $bp < 46 || $bp > 56) return false;

        return array('latitude' => $bp, 'longitude' => $lp);
    }

    private function pot2wgs($latitude, $longitude)
    {
        /* Copyright (c) 2006, HELMUT H. HEIMEIER
           Permission is hereby granted, free of charge, to any person obtaining a
           copy of this software and associated documentation files (the "Software"),
           to deal in the Software without restriction, including without limitation
           the rights to use, copy, modify, merge, publish, distribute, sublicense,
           and/or sell copies of the Software, and to permit persons to whom the
           Software is furnished to do so, subject to the following conditions:
           The above copyright notice and this permission notice shall be included
           in all copies or substantial portions of the Software.

           Changes by Martin Herrlein: Input and return values. Removing globals.
           Returns array
        */

        /* Die Funktion verschiebt das Kartenbezugssystem (map datum) vom in
           Deutschland gebräuchlichen Potsdam-Datum zum WGS84 (World Geodetic
           System 84) Datum. Geographische Länge lp und Breite bp gemessen in
           grad auf dem Bessel-Ellipsoid müssen gegeben sein.
           Ausgegeben werden geographische Länge lw und
           Breite bw (in grad) auf dem WGS84-Ellipsoid.
           Bei der Transformation werden die Ellipsoidachsen parallel
           verschoben um dx = 587 m, dy = 16 m und dz = 393 m. */

        $lp = $longitude;
        $bp = $latitude;

        // Geographische Länge lp und Breite bp im Potsdam Datum
        if ($lp == "" || $bp == "") return false;

        $lp = doubleval($lp);
        $bp = doubleval($bp);

        // Quellsystem Potsdam Datum
        //  Große Halbachse a und Abplattung fq
        $a = 6378137.000 - 739.845;
        $fq = 3.35281066e-3 - 1.003748e-05;

        // Zielsystem WGS84 Datum
        //  Abplattung f
        $f = 3.35281066e-3;

        // Parameter für datum shift
        $dx = 587;
        $dy = 16;
        $dz = 393;

        // Quadrat der ersten numerischen Exzentrizität in Quell- und Zielsystem
        $e2q = (2 * $fq - $fq * $fq);
        $e2 = (2 * $f - $f * $f);

        // Breite und Länge in Radianten
        $b1 = $bp * (pi() / 180);
        $l1 = $lp * (pi() / 180);

        // Querkrümmungshalbmesser nd
        $nd = $a / sqrt(1 - $e2q * sin($b1) * sin($b1));

        // Kartesische Koordinaten des Quellsystems Potsdam
        $xp = $nd * cos($b1) * cos($l1);
        $yp = $nd * cos($b1) * sin($l1);
        $zp = (1 - $e2q) * $nd * sin($b1);

        // Kartesische Koordinaten des Zielsystems (datum shift) WGS84
        $x = $xp + $dx;
        $y = $yp + $dy;
        $z = $zp + $dz;

        // Berechnung von Breite und Länge im Zielsystem
        $rb = sqrt($x * $x + $y * $y);
        $b2 = (180 / pi()) * atan(($z / $rb) / (1 - $e2));

        if ($x > 0)
            $l2 = (180 / pi()) * atan($y / $x);
        if ($x < 0 && $y > 0)
            $l2 = (180 / pi()) * atan($y / $x) + 180;
        if ($x < 0 && $y < 0)
            $l2 = (180 / pi()) * atan($y / $x) - 180;

        $lw = $l2;
        $bw = $b2;
        return array('latitude' => $bw, 'longitude' => $lw);
    }
}
