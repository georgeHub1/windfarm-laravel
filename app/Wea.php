<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wea extends Model
{
    protected $table = 'wea';
    protected $fillable =
        [
            'akz',
            'dataid',
            'serialnr',
            'weanr',
            'mowing',
            'mahd_days',
            'crane',
            'kranich_start',
            'kranich_stop',
            'military',
            'bird',
            'milan',
            'gkRight',
            'gkHeight',
            'latitude',
            'longitude',
            'mahd_offset_sunrise',
            'mahd_offset_sunset',
            'update',
            'deleted'
        ];
}
