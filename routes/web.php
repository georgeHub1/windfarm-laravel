<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->middleware('auth');

//AKZ Manager
Route::get('/manageAkz', 'AkzManagerController@manage')->name('akzManager')->middleware('auth');
Route::post('/addAkz', 'AkzManagerController@addAkz')->middleware('auth');
Route::get('/delAkz', 'AkzManagerController@delAkz')->middleware('auth');
Route::post('/editAkz', 'AkzManagerController@editAkz')->middleware('auth');

//WEA Manager
Route::get('/manageWea', 'WeaManagerController@manage')->name('weaManager')->middleware('auth');
Route::post('/addWea', 'WeaManagerController@addWea')->middleware('auth');
Route::get('/delWea', 'WeaManagerController@delWea')->middleware('auth');
Route::post('/editWea', 'WeaManagerController@editWea')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Admin List
Route::get('/adminList', 'AdminListController@showList')->middleware('auth');
Route::get('/delAdmin', 'AdminListController@deleteAdmin')->middleware('auth');